(function() {
    angular.module('app').directive('contenteditable', ['$sce',
        function($sce) {
            var cursorId;
            var emojis = [{
                name: "smile",
                patterns: [/:\)/g, /:smile:/g],
                imageUrl: "images/smile.png"
            }, {
                name: "heart",
                patterns: [/&lt;3/g],
                imageUrl: "images/heart.jpeg"
            }];

            function setCaretPosition(id) {
                if (id) {
                    var selection= window.getSelection();
                    if (selection.rangeCount >0){
                        selection.removeAllRanges();
                    }
                    var range = document.createRange();
                    var element=document.getElementById(id);
                    range.selectNode(element);
                    range.collapse();
                    selection.addRange(range);
                }
            }

            function replaceEmojis(html) {
                angular.forEach(emojis, function(value, index) {
                    angular.forEach(value.patterns, function(pattern, patternIndex) {
                        var matchedStrings = html.match(pattern);
                        angular.forEach(matchedStrings, function(mString, index) {
                            var url = mString;
                            var stringPos=html.indexOf(mString);
                            cursorId=Math.random().toString(36).substring(7);
                            var replaceString = '<img id="'+cursorId+'"class="emoji" src="' + value.imageUrl + '">';
                            html = html.replace(mString, replaceString);
                        });
                    });
                });
                return html;
            }
            return {
                restrict: 'A',
                require: '?ngModel',
                link: function(scope, element, attrs, ngModel) {
                    if(scope.configEmojis&&scope.configEmojis.length>0 ){
                        emojis=emojis.concat(scope.configEmojis);
                    }

                    if (!ngModel) return;


                    ngModel.$render = function() {
                        element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
                    };

                    // Listen for change events to enable binding
                    element.on('keyup', function(event) {
                        if (event.keyCode == 13) {
                            read(true);
                            return false;
                        } else {
                            read();
                            return true;
                        }
                    });
                    read(); // initialize

                    // Write data to the model
                    function read(push) {
                        var html = element.html();
                        if (attrs.stripBr && html == '<br>') {
                            html = '';
                        }
                        if (push) {
                            html=html.replace("<div><br></div>","");
                            if (scope.htmls) {
                                scope.htmls.push($sce.trustAsHtml(html));
                            } else {
                                scope.htmls = [$sce.trustAsHtml(html)];
                            }
                            ngModel.$setViewValue("");
                            element.html("");
                        } else {
                            if(html != replaceEmojis(html)){
                                 html = replaceEmojis(html);
                                element.html(html);
                                ngModel.$setViewValue(html);
                                setCaretPosition(cursorId);
                            }
                        }
                    }
                }
            };
        }
    ]);
})();
