 (function(){
    angular.module('app',[]).controller('homeController', ['$scope', function($scope){
         $scope.configEmojis = [{
                name: "kiss",
                patterns: [/:\*/g,/:kiss:/g],
                imageUrl: "images/kiss.jpeg"
            }, {
                name: "cool",
                patterns: [/B\|/g,/:cool:/],
                imageUrl: "images/cool.jpeg"
            }
        ];
    }]);
 })();
